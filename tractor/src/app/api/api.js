import HttpService from './http-service';

const API = {

    getCars() {
        return HttpService.get('/cars');
    },

    findCars(payload) {
        return HttpService.get('/cars/find', payload);
    },

    addCar(payload) {
        return HttpService.post('/cars/new', payload);
    }
};

export default API;