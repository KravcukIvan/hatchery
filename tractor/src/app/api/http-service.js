import axios from 'axios';
import {environment} from './../environment';

const HttpService =  {

    get(endPoint, options){
        const api = environment.host;
        return axios.get(api + endPoint, options);
    },

    post(endPoint, options){
        const api = environment.host;
        return axios.post(api + endPoint, options);
    }
};

export default HttpService;