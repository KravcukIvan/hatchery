import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Header from './shared/header/header';
import Footer from './shared/footer/footer';
import About from './pages/about/about';
import TractorList from './pages/tractor-list/tractor-list';
import './app.scss';

class App extends Component {

    render() {
        return (
            <Router>
                <div className="container">
                    <Header/>
                    <Switch>
                        <Route exact path='/' component={TractorList} />
                        <Route path='/about' component={About} />
                    </Switch>
                    <Footer/>
                </div>
            </Router>
        );
    }
}

export default App;
