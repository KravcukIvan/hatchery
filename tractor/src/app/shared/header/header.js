import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';

import tractor from '../../../assets/images/traktor.jpg';

class Header extends Component {

    render() {
        return (
            <Fragment>
                <nav className='navbar navbar-dark bg-dark'>
                    <a className='navbar-brand' href='/'>Půjčovna traktorů</a>
                </nav>
                <header className='row align-items-center my-5'>
                    <div className='col-lg-7'>
                        <img src={tractor} className='img-fluid rounded mb-4 mb-lg-0' alt="tractor"/>
                    </div>
                    <div className='col-lg-5'>
                        <h1 className='font-weight-light'>Půjčovna traktorů</h1>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, ipsam, eligendi,
                            in quo sunt possimus non incidunt odit vero aliquid similique quaerat nam nobis
                            illo aspernatur
                            vitae fugiat numquam repellat.
                        </p>
                        <Link className='btn btn-primary mr-2' to='/'>Seznam traktorů</Link>
                        <Link className='btn btn-primary' to='/about'>O půjčovně</Link>
                    </div>
                </header>
            </Fragment>
        );
    }
}
export default Header;