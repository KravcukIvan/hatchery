import React, { Component } from 'react';
import API from '../../api/api';

class TractorList extends Component {

  constructor(props) {
    super(props);

    this.state = {
      feedback: 'loading',
      tractorList: [],
      showNewCarForm: false,
      filtered: false
    };
    this.handleShowNewCarForm = this.handleShowNewCarForm.bind(this);
    this.handleCloseNewCarForm = this.handleCloseNewCarForm.bind(this);
    this.handleAddNewCar = this.handleAddNewCar.bind(this);
    this.handleCancelFilter = this.handleCancelFilter.bind(this);
    this.handleFilterTractors = this.handleFilterTractors.bind(this);
  }

  componentDidMount() {
    this.getCars();
  }

  getCars() {
    API.getCars()
      .then((response) => {
        this.setState({tractorList: response.data, feedback: 'success'});
      })
      .catch((error) => {
        this.setState({feedback: 'error'}, () => console.error('Chyba', error));
      });
  }

  handleShowNewCarForm() {
    this.setState({showNewCarForm: true});
  }

  handleCloseNewCarForm() {
    this.setState({showNewCarForm: false});
  }

  handleAddNewCar(event) {
    event.preventDefault();

    const formData = {
      type: event.target.type.value,
      vin: event.target.vin.value,
      date: (new Date(event.target.date.value)).toISOString(),
      price: event.target.price.value
    };

    this.setState({
      feedback: 'loading'
    }, () => {
      API.addCar(formData)
        .then(() => {
          this.setState((prevState) => {
            return {
              ...prevState,
              feedback: 'success'
            };
          });
        })
        .catch((error) => {
          this.setState((prevState) => {
            return {
              ...prevState,
              feedback: 'error'
            };
          }, () => console.error('Chyba', error));
        });
    });
  }

  handleFilterTractors(event) {
    event.preventDefault();

    const formData = {
      acquiredFrom: (new Date(event.target.acquiredFrom.value)).toISOString(),
      acquiredTo: (new Date(event.target.acquiredTo.value)).toISOString()
    };

    API.findCars(formData)
      .then((response) => {
        this.setState((prevState) => {
          return {
            ...prevState,
            tractorList: response.data,
            feedback: 'success',
            filtered: true
          };
        });
      })
      .catch((error) => {
        this.setState((prevState) => {
          return {
            ...prevState,
            feedback: 'error'
          };
        }, () => console.error('Chyba', error));
      });
  }

  handleCancelFilter() {
    this.setState({
      feedback: 'loading',
      filtered: false
    }, () => this.getCars());
  }

  getFilter() {
    return (
      <form className='mt-2' onSubmit={this.handleFilterTractors}>
        <div className='form-row'>
          <div className='form-group col-3'>
            <label htmlFor='acquiredFrom' className='pr-2'>Od</label>
            <input type="text" defaultValue="12.02.2018" name="acquiredFrom" className='form-control'/>
          </div>
          <div className='form-group col-3'>
            <label htmlFor='acquiredTo' className='pr-2'>Do</label>
            <input type="text" defaultValue="12.02.2019" name="acquiredTo" className='form-control'/>
          </div>
          <div className='form-group col-3 d-flex align-items-end'>
            <div>
              <button type='submit' className='btn btn-primary mr-2'>
                Filtruj
              </button>
              <button disabled={!this.state.filtered} onClick={this.handleCancelFilter}
                      className='btn btn-outline-secondary'>
                Zruš filtr
              </button>
            </div>
          </div>
        </div>
      </form>
    );
  }

  getFilterResult() {
    let result = null;
    if (this.state.tractorList && this.state.tractorList.length > 0) {
      result = (
        <table className='table table-striped'>
          <thead>
          <tr>
            <th>ID</th>
            <th>Type</th>
            <th>VIN</th>
            <th>State</th>
          </tr>
          </thead>
          <tbody>
          {
            this.state.tractorList.map((tractor, idx) => {
              return (
                <tr key={idx}>
                  <td>{tractor.id}</td>
                  <td>{tractor.type}</td>
                  <td>{tractor.vin}</td>
                  <td>{tractor.carState}</td>
                </tr>
              );
            })
          }
          </tbody>
        </table>
      );
    } else {
      result = (
        <p>Není tu žádný traktor</p>
      );
    }
    return result;
  }

  getNewForm() {
    let result = null;
    if (this.state.showNewCarForm) {
      result = (
        <div className='card mb-3'>
          <div className='card-header'>Přidání nového vozidla</div>
          <div className='card-body'>
            <form onSubmit={this.handleAddNewCar}>
              <div className='form-row mb-1'>
                <div className='col'>
                  <label htmlFor='carType'>Type</label>
                  <input type='text' name='type' className='form-control' id='carType'
                         defaultValue='RECLAIMER'/>
                </div>
                <div className='col'>
                  <label htmlFor='vin'>VIN</label>
                  <input type='text' name='vin' className='form-control' id='vin'
                         defaultValue='AHTBB3QD001726541'/>
                </div>
              </div>
              <div className='form-row mb-1'>
                <div className='col custom-date'>
                  <label htmlFor='date'>Datum</label>
                  <input type="text" name="date" defaultValue='12.01.2019' id='date'
                         className='form-control'/>
                </div>
                <div className='col'>
                  <label htmlFor='price'>Cena</label>
                  <input type='text' name='price' className='form-control'
                         id='price' defaultValue='110000'/>
                </div>
              </div>
              <div className='col text-right'>
                <button onClick={this.handleCloseNewCarForm} className='btn btn-warning mr-2'>
                  Zavřit
                </button>
                <button type='submit' className='btn btn-success'>
                  Ulož
                </button>
              </div>
            </form>
          </div>
        </div>
      );
    }
    return result;
  }

  getPanelHeader() {
    const button = !this.state.showNewCarForm && (
      <button onClick={this.handleShowNewCarForm} type='button' className='btn btn-info'>
        Přidat
      </button>
    );
    return (
      <div className='card-header d-flex align-items-center justify-content-between'>
        Seznam vozidel {button}
      </div>
    );
  }

  getTractors() {
    return (
      <div className='card'>
        {this.getPanelHeader()}
        <div className='card-body'>
          {this.getFilter()}
          {this.getFilterResult()}
        </div>
      </div>
    );
  }

  render() {
    return (
      <section className='py-2'>
        <h2>Seznam traktorů (vozidel)</h2>
        {this.getNewForm()}
        {this.getTractors()}
        <div className='text-center'>
          {
            this.state.feedback === 'error' && (
              <div className='alert alert-danger my-2' role='alert'>Došlo k chybě</div>
            )
          }
          {
            this.state.feedback === 'loading' && (
              <div className='spinner-border text-primary my-2' role='status'>
                <span className='sr-only'>Loading...</span>
              </div>
            )
          }
        </div>
      </section>
    );
  }
}

export default TractorList;