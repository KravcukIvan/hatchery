import React, { Component, Fragment } from 'react';

class About extends Component {
    render() {
        return (
            <Fragment>
                <section className='py-2'>
                    <div className='container'>
                        <h2>O půjčovně</h2>
                        <p className='lead'>Lorem ipsum dolor sit amet, consectetur adipisicing elit*</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, suscipit, rerum quos
                            facilis repellat architecto commodi officia atque nemo facere eum non illo voluptatem
                            quae delectus odit vel itaque amet.
                        </p>
                    </div>
                </section>
            </Fragment>
        );
    }
}

export default About;