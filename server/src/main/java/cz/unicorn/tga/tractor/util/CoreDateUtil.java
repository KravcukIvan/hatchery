/**
 * 
 */
package cz.unicorn.tga.tractor.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author DZCJS9F
 *
 */
public class CoreDateUtil {

	public static LocalDate getToday() {
		return LocalDate.now();
	}

	public static LocalDateTime getNow() {
		return LocalDateTime.now();
	}
}
