/**
 * 
 */
package cz.unicorn.tga.tractor.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.Data;

/**
 * @author DZCJS9F
 *
 */

@Data
public class CarNewForm {

	private String type;
	private String vin;
	private BigDecimal price;
	private LocalDate dateOfAquisition;

}
