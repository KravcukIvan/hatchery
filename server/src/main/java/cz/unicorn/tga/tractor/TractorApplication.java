package cz.unicorn.tga.tractor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

// apart from standard entity scan in the application root package, we need to instruct Spring Boot to scan Jsr310JpaConverters
// in order to register java.time converters
@EntityScan(basePackageClasses = {TractorApplication.class, Jsr310JpaConverters.class})
@SpringBootApplication
public class TractorApplication {

	public static void main(String[] args) {
		SpringApplication.run(TractorApplication.class, args);
	}

	@Bean
	public HibernateJpaSessionFactoryBean sessionFactory() {
		return new HibernateJpaSessionFactoryBean();
	}
}
